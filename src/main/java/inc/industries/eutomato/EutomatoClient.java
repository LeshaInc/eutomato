package inc.industries.eutomato;

import inc.industries.eutomato.conduits.model.ConduitModelResourceProvider;
import inc.industries.eutomato.model.ObjModelResourceProvider;
import inc.industries.eutomato.registry.BlockEntityRendererRegistry;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;

public class EutomatoClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        ModelLoadingRegistry.INSTANCE.registerResourceProvider(ObjModelResourceProvider::new);
        ModelLoadingRegistry.INSTANCE.registerResourceProvider(ConduitModelResourceProvider::new);

        BlockEntityRendererRegistry.register();
    }
}
