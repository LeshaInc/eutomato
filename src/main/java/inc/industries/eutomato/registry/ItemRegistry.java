package inc.industries.eutomato.registry;

import inc.industries.eutomato.conduits.item.ItemConduitItem;

@SuppressWarnings("unused")
public class ItemRegistry {
    public static ItemConduitItem ITEM_CONDUIT;

    public static void register() {
        RegistryUtil.registerItem(ITEM_CONDUIT = new ItemConduitItem(), "item_conduit");
    }
}
