package inc.industries.eutomato.registry;

import inc.industries.eutomato.conduits.ConduitBlockEntityRenderer;

public class BlockEntityRendererRegistry {
    public static void register() {
        RegistryUtil.registerBlockEntityRenderer(BlockEntityRegistry.CONDUIT, ConduitBlockEntityRenderer::new);
    }
}
