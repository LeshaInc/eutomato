package inc.industries.eutomato.registry;

import inc.industries.eutomato.conduits.ConduitBlock;

public class BlockRegistry {
    public static ConduitBlock CONDUIT;

    public static void register() {
        RegistryUtil.registerBlock(CONDUIT = new ConduitBlock(), "conduit");
    }
}
