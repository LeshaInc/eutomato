package inc.industries.eutomato.registry;

import inc.industries.eutomato.Eutomato;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class RegistryUtil {
    public static void registerItem(Item item, String id) {
        Registry.register(Registry.ITEM, new Identifier(Eutomato.MOD_ID, id), item);
    }

    public static void registerBlock(Block block, String id) {
        Registry.register(Registry.BLOCK, new Identifier(Eutomato.MOD_ID, id), block);
    }

    public static <T extends BlockEntity> BlockEntityType<T> registerBlockEntity(FabricBlockEntityTypeBuilder.Factory<T> factory, Block block, String id) {
        return Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(Eutomato.MOD_ID, id),
                FabricBlockEntityTypeBuilder.create(factory, block).build(null));
    }

    @Environment(EnvType.CLIENT)
    public static <T extends BlockEntity> void registerBlockEntityRenderer(BlockEntityType<T> blockEntityType, BlockEntityRendererFactory<? super T> factory) {
        BlockEntityRendererRegistry.INSTANCE.register(blockEntityType, factory);
    }
}
