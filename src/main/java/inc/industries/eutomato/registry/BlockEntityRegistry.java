package inc.industries.eutomato.registry;

import inc.industries.eutomato.conduits.ConduitBlockEntity;
import net.minecraft.block.entity.BlockEntityType;

public class BlockEntityRegistry {
    public static BlockEntityType<ConduitBlockEntity> CONDUIT;

    public static void register() {
        CONDUIT = RegistryUtil.registerBlockEntity(ConduitBlockEntity::new, BlockRegistry.CONDUIT, "conduit");
    }
}
