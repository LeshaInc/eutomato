package inc.industries.eutomato.registry;

import inc.industries.eutomato.Eutomato;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.util.Identifier;

public class SpriteRegistry {
    public static final SpriteIdentifier CONDUIT_PARTICLE = mkIdent("conduit-particle");
    public static final SpriteIdentifier CONDUIT_ITEM_BELT = mkIdent("conduit-belt");

    private static SpriteIdentifier mkIdent(String id) {
        return new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, new Identifier(Eutomato.MOD_ID, id));
    }
}
