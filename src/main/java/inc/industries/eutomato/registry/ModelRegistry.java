package inc.industries.eutomato.registry;

import inc.industries.eutomato.Eutomato;
import net.minecraft.util.Identifier;

public class ModelRegistry {
    public static final Identifier ITEM_CONDUIT_STRAIGHT = mkIdent("conduit/item-straight.obj");
    public static final Identifier ITEM_CONDUIT_TURN = mkIdent("conduit/item-turn.obj");
    public static final Identifier ITEM_CONDUIT_3WAY = mkIdent("conduit/item-3way.obj");
    public static final Identifier ITEM_CONDUIT_4WAY = mkIdent("conduit/item-4way.obj");

    private static Identifier mkIdent(String id) {
        return new Identifier(Eutomato.MOD_ID, id);
    }
}
