package inc.industries.eutomato.conduits.model;

import inc.industries.eutomato.conduits.ConduitPartRegistry;
import inc.industries.eutomato.conduits.ConduitPartRenderer;
import inc.industries.eutomato.conduits.ConduitRenderData;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.api.rendering.data.v1.RenderAttachedBlockView;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockRenderView;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

@SuppressWarnings("ClassCanBeRecord")
public class ConduitBakedModel implements BakedModel, FabricBakedModel {
    private final Sprite particleSprite;

    public ConduitBakedModel(Sprite particleSprite) {
        this.particleSprite = particleSprite;
    }

    @Override
    public boolean isVanillaAdapter() {
        return false;
    }

    @Override
    public void emitBlockQuads(BlockRenderView blockView_, BlockState state, BlockPos pos, Supplier<Random> randomSupplier, RenderContext context) {
        var blockView = (RenderAttachedBlockView) blockView_;
        var data = (ConduitRenderData) blockView.getBlockEntityRenderAttachment(pos);
        if (data == null) return;

        for (var entry : data.dataMap.entrySet()) {
            var renderer = (ConduitPartRenderer) ConduitPartRegistry.getRenderer(entry.getKey());
            renderer.renderStatic(entry.getValue(), blockView, state, pos, randomSupplier, context);
        }
    }

    @Override
    public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context) {
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction face, Random random) {
        return null;
    }

    @Override
    public boolean useAmbientOcclusion() {
        return true;
    }

    @Override
    public boolean hasDepth() {
        return true;
    }

    @Override
    public boolean isSideLit() {
        return false;
    }

    @Override
    public boolean isBuiltin() {
        return false;
    }

    @Override
    public Sprite getSprite() {
        return particleSprite;
    }

    @Override
    public ModelTransformation getTransformation() {
        return null;
    }

    @Override
    public ModelOverrideList getOverrides() {
        return null;
    }
}
