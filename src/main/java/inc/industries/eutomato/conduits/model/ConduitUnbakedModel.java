package inc.industries.eutomato.conduits.model;

import com.mojang.datafixers.util.Pair;
import inc.industries.eutomato.conduits.ConduitPartRegistry;
import inc.industries.eutomato.registry.SpriteRegistry;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConduitUnbakedModel implements UnbakedModel {
    @Override
    public Collection<Identifier> getModelDependencies() {
        return ConduitPartRegistry.getRegisteredIds().stream()
                .flatMap(partId -> ConduitPartRegistry.getRenderer(partId).getModelIdentifiers().stream())
                .collect(Collectors.toSet());
    }

    private Stream<SpriteIdentifier> additionalTextures() {
        return Stream.concat(
                ConduitPartRegistry.getRegisteredIds().stream()
                        .flatMap(partId -> ConduitPartRegistry.getRenderer(partId).getSpriteIdentifiers().stream()),
                Stream.of(SpriteRegistry.CONDUIT_PARTICLE));
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter, Set<Pair<String, String>> unresolvedTextureReferences) {
        return Stream.concat(
                getModelDependencies().stream()
                        .flatMap(dep -> unbakedModelGetter
                                .apply(dep)
                                .getTextureDependencies(unbakedModelGetter, unresolvedTextureReferences)
                                .stream()
                        ),
                additionalTextures()).collect(Collectors.toSet());
    }

    @Nullable
    @Override
    public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings rotationContainer, Identifier modelId) {
        var models = getModelDependencies().stream()
                .map(dep -> new Pair<>(dep, (FabricBakedModel) loader.bake(dep, rotationContainer)))
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));
        var sprites = additionalTextures()
                .collect(Collectors.toMap(id -> id, textureGetter));
        for (var id : ConduitPartRegistry.getRegisteredIds()) {
            var renderer = ConduitPartRegistry.getRenderer(id);
            renderer.receiveModels(models);
            renderer.receiveSprites(sprites);
        }
        return new ConduitBakedModel(sprites.get(SpriteRegistry.CONDUIT_PARTICLE));
    }
}
