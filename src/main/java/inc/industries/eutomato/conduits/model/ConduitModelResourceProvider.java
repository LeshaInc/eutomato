package inc.industries.eutomato.conduits.model;

import inc.industries.eutomato.Eutomato;
import net.fabricmc.fabric.api.client.model.ModelProviderContext;
import net.fabricmc.fabric.api.client.model.ModelResourceProvider;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

public class ConduitModelResourceProvider implements ModelResourceProvider {
    public static final Identifier IDENTIFIER = new Identifier(Eutomato.MOD_ID, "conduit");

    public ConduitModelResourceProvider(@SuppressWarnings("unused") ResourceManager manager) {
    }

    @Override
    public @Nullable UnbakedModel loadModelResource(Identifier id, ModelProviderContext context) {
        if (id.equals(IDENTIFIER)) {
            return new ConduitUnbakedModel();
        }
        return null;
    }
}
