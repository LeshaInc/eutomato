package inc.industries.eutomato.conduits;

import java.util.Map;
import java.util.stream.Collectors;

public class ConduitRenderData {
    public final Map<String, Object> dataMap;

    public ConduitRenderData(Map<String, ConduitPart> parts) {
        dataMap = parts.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getRenderData()));
    }
}
