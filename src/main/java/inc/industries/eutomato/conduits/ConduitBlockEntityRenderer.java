package inc.industries.eutomato.conduits;

import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;

public class ConduitBlockEntityRenderer implements BlockEntityRenderer<ConduitBlockEntity> {
    public ConduitBlockEntityRenderer(BlockEntityRendererFactory.Context ctx) {

    }

    @Override
    public void render(ConduitBlockEntity entity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        for (var id : ConduitPartRegistry.getRegisteredIds()) {
            var renderer = ConduitPartRegistry.getRenderer(id);
            renderer.renderDynamic(entity, tickDelta, matrices, vertexConsumers, light, overlay);
        }
    }
}
