package inc.industries.eutomato.conduits.item;

import net.minecraft.util.Identifier;
import net.minecraft.util.math.Direction;

public record ItemConduitRenderData(Identifier modelId, Direction direction) {
}
