package inc.industries.eutomato.conduits.item;

import inc.industries.eutomato.conduits.ConduitBlockEntity;
import inc.industries.eutomato.conduits.ConduitItem;
import inc.industries.eutomato.conduits.ConduitPart;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

public class ItemConduitItem extends ConduitItem {
    @Override
    protected ConduitPart newPart(BlockPos pos, ItemUsageContext context) {
        var direction = context.getPlayer() != null ? context.getPlayer().getHorizontalFacing() : Direction.NORTH;
        return new ItemConduitPart(context.getWorld(), pos, direction);
    }

    @Override
    protected ActionResult addPart(ConduitBlockEntity conduit, ConduitPart part) {
        conduit.addPart("item", part);
        return ActionResult.SUCCESS;
    }
}
