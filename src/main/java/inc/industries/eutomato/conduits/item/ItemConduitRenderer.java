package inc.industries.eutomato.conduits.item;

import inc.industries.eutomato.conduits.ConduitBlockEntity;
import inc.industries.eutomato.conduits.ConduitPartRenderer;
import inc.industries.eutomato.registry.ModelRegistry;
import inc.industries.eutomato.registry.SpriteRegistry;
import inc.industries.eutomato.util.RenderUtil;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.api.rendering.data.v1.RenderAttachedBlockView;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.*;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

public class ItemConduitRenderer implements ConduitPartRenderer {
    private Map<Identifier, FabricBakedModel> models;

    @Override
    public List<Identifier> getModelIdentifiers() {
        return List.of(ModelRegistry.ITEM_CONDUIT_STRAIGHT, ModelRegistry.ITEM_CONDUIT_TURN,
                ModelRegistry.ITEM_CONDUIT_3WAY, ModelRegistry.ITEM_CONDUIT_4WAY);
    }

    @Override
    public List<SpriteIdentifier> getSpriteIdentifiers() {
        return List.of(SpriteRegistry.CONDUIT_ITEM_BELT);
    }

    public void receiveModels(Map<Identifier, FabricBakedModel> models) {
        this.models = models;
    }

    @Override
    public void renderStatic(Object rawData, RenderAttachedBlockView blockView, BlockState state, BlockPos pos,
                             Supplier<Random> randomSupplier, RenderContext context) {
        var data = (ItemConduitRenderData) rawData;
        var model = models.get(data.modelId());
        RenderUtil.pushSimpleTransform(data.direction(), context);
        model.emitBlockQuads(blockView, state, pos, randomSupplier, context);
        context.popTransform();
    }

    @Override
    public void renderDynamic(ConduitBlockEntity conduit, float tickDelta, MatrixStack matrices,
                              VertexConsumerProvider vertexConsumers, int light, int overlay) {
        var itemConduit = (ItemConduitPart) conduit.getPart("item");
        if (itemConduit == null) return;

        var buffer = SpriteRegistry.CONDUIT_ITEM_BELT.getVertexConsumer(vertexConsumers, RenderLayer::getEntityCutout);

        var time = ((float) (conduit.getWorld() != null ? conduit.getWorld().getTime() : 0)) + tickDelta;
        var vOffset = time / 20f * 1024f / 465f % 4f;

        var numConnections = itemConduit.numNonemptyConnections();

        for (var i = 0; i < 4; i++) {
            var direction = Direction.fromHorizontal(i);
            var connection = itemConduit.getConnection(direction);
            if (connection == ItemConduitPart.ConnectionKind.NONE) continue;

            // TODO: cache the hell out of it
            matrices.push();
            matrices.translate(0.5f, 0f, 0.5f);
            matrices.multiply(new Quaternion(Vec3f.POSITIVE_Y, 180f - i * 90f, true));
            matrices.translate(-0.5f, 0f, -0.5f);
            matrices.scale(1f, 1f, 0.375f);
            if (connection == ItemConduitPart.ConnectionKind.SRC_BELT) {
                matrices.translate(0.5f, 0f, -0.5f);
                matrices.multiply(new Quaternion(Vec3f.POSITIVE_Y, 180f, true));
                matrices.translate(-0.5f, 0f, -1.5f);
            }
            renderBelt(vOffset, 0.375f, matrices.peek(), buffer, light, overlay);
            matrices.pop();

            if (numConnections == 2 && itemConduit.getConnection(direction) == ItemConduitPart.ConnectionKind.DST_BELT
                    && (itemConduit.getConnection(direction.getOpposite()) == ItemConduitPart.ConnectionKind.SRC_BELT
                    || itemConduit.getConnection(direction.rotateYClockwise()) == ItemConduitPart.ConnectionKind.SRC_BELT
                    || itemConduit.getConnection(direction.rotateYCounterclockwise()) == ItemConduitPart.ConnectionKind.SRC_BELT)) {
                matrices.push();
                matrices.translate(0.5f, 0f, 0.5f);
                matrices.multiply(new Quaternion(Vec3f.POSITIVE_Y, 180f - i * 90f, true));
                matrices.translate(-0.5f, 0f, -0.5f);
                matrices.scale(1f, 1f, 0.25f);
                matrices.translate(0f, 0f, 1.5f);
                renderBelt(vOffset, 0.25f, matrices.peek(), buffer, light, overlay);
                matrices.pop();
            }
        }
    }

    private void renderBelt(float vOffset, float vLength,
                            MatrixStack.Entry matrix, VertexConsumer buffer, int light, int overlay) {
        var modelMatrix = matrix.getModel();
        var normalMatrix = matrix.getNormal();
        Vec3f normal = new Vec3f();
        Vector4f pos = new Vector4f();

        @FunctionalInterface
        interface VertexEmitter {
            void emit(float x, float y, float z, float u, float v);
        }

        VertexEmitter vertexEmitter = (x, y, z, u, v) -> {
            pos.set(x, y, z, 1f);
            pos.transform(modelMatrix);
            buffer.vertex(pos.getX(), pos.getY(), pos.getZ(), 1f, 1f, 1f, 1f, u, (v + vOffset) / 8f, overlay, light, normal.getX(), normal.getY(), normal.getZ());
        };

        normal.set(0f, 1f, 0f);
        normal.transform(normalMatrix);
        vertexEmitter.emit(0.375f, 0.5f, 1.0f, 0f, vLength);
        vertexEmitter.emit(0.625f, 0.5f, 1.0f, 1f / 4f, vLength);
        vertexEmitter.emit(0.625f, 0.5f, 0.0f, 1f / 4f, 0f);
        vertexEmitter.emit(0.375f, 0.5f, 0.0f, 0f, 0f);

        normal.set(-1f, 0f, 0f);
        normal.transform(normalMatrix);
        vertexEmitter.emit(0.375f, 0.46875f, 1.0f, 0f, vLength);
        vertexEmitter.emit(0.375f, 0.5f, 1.0f, 1f / 16f, vLength);
        vertexEmitter.emit(0.375f, 0.5f, 0.0f, 1f / 16f, 0f);
        vertexEmitter.emit(0.375f, 0.46875f, 0.0f, 0f, 0f);

        normal.set(1f, 0f, 0f);
        normal.transform(normalMatrix);
        vertexEmitter.emit(0.625f, 0.46875f, 0.0f, 0f, vLength);
        vertexEmitter.emit(0.625f, 0.5f, 0.0f, 1f / 16f, vLength);
        vertexEmitter.emit(0.625f, 0.5f, 1.0f, 1f / 16f, 0f);
        vertexEmitter.emit(0.625f, 0.46875f, 1.0f, 0f, 0f);

        normal.set(0f, 0f, -1f);
        normal.transform(normalMatrix);
        vertexEmitter.emit(0.375f, 0.5f, 0.0f, 0f, 1f / 16f);
        vertexEmitter.emit(0.625f, 0.5f, 0.0f, 1f, 1f / 16f);
        vertexEmitter.emit(0.625f, 0.46875f, 0.0f, 1f, 0f);
        vertexEmitter.emit(0.375f, 0.46875f, 0.0f, 0f, 0f);

        normal.set(0f, 0f, 1f);
        normal.transform(normalMatrix);
        vertexEmitter.emit(0.375f, 0.46875f, 1.0f, 0f, vLength);
        vertexEmitter.emit(0.625f, 0.46875f, 1.0f, 1f / 4f, vLength);
        vertexEmitter.emit(0.625f, 0.5f, 1.0f, 1f / 4f, vLength - 1f / 16f);
        vertexEmitter.emit(0.375f, 0.5f, 1.0f, 0f, 1f - 1f / 16f);
    }
}
