package inc.industries.eutomato.conduits.item;

import inc.industries.eutomato.conduits.ConduitBlockEntity;
import inc.industries.eutomato.conduits.ConduitPart;
import inc.industries.eutomato.registry.BlockEntityRegistry;
import inc.industries.eutomato.registry.ModelRegistry;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ItemConduitPart implements ConduitPart {
    private final static VoxelShape CENTER_SHAPE = VoxelShapes.cuboid(0.3125f, 0.3125f, 0.3125f, 0.6875f, 0.5f, 0.6875f);
    private final static VoxelShape CENTER_TALL_SHAPE = VoxelShapes.cuboid(0.3125f, 0.3125f, 0.3125f, 0.6875f, 0.6875f, 0.6875f);
    private final static VoxelShape EAST_SHAPE = VoxelShapes.cuboid(0.6875f, 0.3125f, 0.3125f, 1f, 0.5f, 0.6875f);
    private final static VoxelShape WEST_SHAPE = VoxelShapes.cuboid(0f, 0.3125f, 0.3125f, 0.3125f, 0.5f, 0.6875f);
    private final static VoxelShape NORTH_SHAPE = VoxelShapes.cuboid(0.3125f, 0.3125f, 0f, 0.6875f, 0.5f, 0.3125f);
    private final static VoxelShape SOUTH_SHAPE = VoxelShapes.cuboid(0.3125f, 0.3125f, 0.6875f, 0.6875f, 0.5f, 1f);

    private final Map<Direction, ConnectionKind> connections = Arrays.stream(Direction.values())
            .collect(Collectors.toMap(dir -> dir, dir -> ConnectionKind.NONE));

    private Identifier modelId = ModelRegistry.ITEM_CONDUIT_STRAIGHT;
    private Direction modelDirection = Direction.NORTH;

    private VoxelShape voxelShape = VoxelShapes.fullCube();

    private final List<BeltRegion> beltRegions = new ArrayList<>(4);

    public ItemConduitPart() {
        update();
    }

    public ItemConduitPart(World world, BlockPos pos, Direction frontDir) {
        var backDir = frontDir.getOpposite();
        var leftDir = frontDir.rotateYCounterclockwise();
        var rightDir = frontDir.rotateYClockwise();

        var frontConduit = getItemConduitAt(world, pos.offset(frontDir));
        if (frontConduit != null) {
            frontConduit.setConnection(backDir, ConnectionKind.SRC_BELT);
            frontConduit.update();
        }

        var backConduit = getItemConduitAt(world, pos.offset(backDir));
        if (backConduit != null) {
            backConduit.setConnection(frontDir, ConnectionKind.DST_BELT);
            backConduit.update();
        }

        var leftConduit = getItemConduitAt(world, pos.offset(leftDir));
        var hasLeft = leftConduit != null && leftConduit.getConnection(rightDir) == ConnectionKind.DST_BELT;
        if (hasLeft) {
            setConnection(leftDir, ConnectionKind.SRC_BELT);
        }

        var rightConduit = getItemConduitAt(world, pos.offset(rightDir));
        var hasRight = rightConduit != null && rightConduit.getConnection(leftDir) == ConnectionKind.DST_BELT;
        if (hasRight) {
            setConnection(rightDir, ConnectionKind.SRC_BELT);
        }

        setConnection(frontDir, ConnectionKind.DST_BELT);

        if ((!hasLeft && !hasRight) || (backConduit != null && backConduit.getConnection(frontDir) == ConnectionKind.DST_BELT)) {
            setConnection(backDir, ConnectionKind.SRC_BELT);
        }

        update();
    }

    private static @Nullable ItemConduitPart getItemConduitAt(World world, BlockPos pos) {
        if (world.getBlockEntity(pos) instanceof ConduitBlockEntity conduit) {
            return (ItemConduitPart) conduit.getPart("item");
        }
        return null;
    }

    public void setConnection(Direction dir, ConnectionKind connection) {
        connections.put(dir, connection);
    }

    public long numNonemptyConnections() {
        return connections.values().stream().filter(c -> c != ConnectionKind.NONE).count();
    }

    public ConnectionKind getConnection(Direction dir) {
        return connections.get(dir);
    }

    private void update() {
        updateModel();
        updateShape();
        updateBeltRegions();
    }

    private void updateModel() {
        var directions = connections.entrySet().stream()
                .filter(e -> e.getValue() != ConnectionKind.NONE)
                .map(Map.Entry::getKey)
                .iterator();
        var numConnections = numNonemptyConnections();

        if (numConnections == 2) {
            var dirA = directions.next();
            var dirB = directions.next();

            if (dirA.getOpposite().equals(dirB)) {
                modelDirection = getConnection(dirA) == ConnectionKind.DST_BELT ? dirA : dirB;
                modelId = ModelRegistry.ITEM_CONDUIT_STRAIGHT;
            } else {
                modelDirection = (dirA.rotateYClockwise().equals(dirB) ? dirA : dirB).getOpposite();
                modelId = ModelRegistry.ITEM_CONDUIT_TURN;
            }
        } else if (numConnections == 3) {
            var dirA = directions.next();
            var dirB = directions.next();
            var dirC = directions.next();

            if (dirA.getOpposite().equals(dirC)) {
                var temp = dirB;
                dirB = dirC;
                dirC = temp;
            } else if (dirB.getOpposite().equals(dirC)) {
                var temp = dirA;
                dirA = dirC;
                dirC = temp;
            }

            modelDirection = dirA.rotateYClockwise().equals(dirC) ? dirA : dirB;
            modelId = ModelRegistry.ITEM_CONDUIT_3WAY;
        } else if (numConnections == 4) {
            modelId = ModelRegistry.ITEM_CONDUIT_4WAY;
        }
    }

    private void updateShape() {
        var shape = numNonemptyConnections() > 2 ? CENTER_TALL_SHAPE : CENTER_SHAPE;

        if (getConnection(Direction.EAST) != ConnectionKind.NONE) {
            shape = VoxelShapes.union(shape, EAST_SHAPE);
        }
        if (getConnection(Direction.WEST) != ConnectionKind.NONE) {
            shape = VoxelShapes.union(shape, WEST_SHAPE);
        }
        if (getConnection(Direction.NORTH) != ConnectionKind.NONE) {
            shape = VoxelShapes.union(shape, NORTH_SHAPE);
        }
        if (getConnection(Direction.SOUTH) != ConnectionKind.NONE) {
            shape = VoxelShapes.union(shape, SOUTH_SHAPE);
        }

        voxelShape = shape;
    }

    private void updateBeltRegions() {
        beltRegions.clear();

        var numConnections = numNonemptyConnections();
        for (var i = 0; i < 4; i++) {
            var dir = Direction.fromHorizontal(i);
            if (getConnection(dir) == ConnectionKind.NONE) continue;
            beltRegions.add(new BeltRegion(0f, 0.375f, dir, getConnection(dir) == ConnectionKind.SRC_BELT));
            if (numConnections == 2 && getConnection(dir) == ConnectionKind.DST_BELT
                    && (getConnection(dir.getOpposite()) == ConnectionKind.SRC_BELT
                    || getConnection(dir.rotateYClockwise()) == ConnectionKind.SRC_BELT
                    || getConnection(dir.rotateYCounterclockwise()) == ConnectionKind.SRC_BELT)) {
                beltRegions.add(new BeltRegion(0.375f, 0.25f, dir, false));
            }
        }
    }

    @Override
    public VoxelShape getCollisionShape() {
        return voxelShape;
    }

    @Override
    public void tick(World world, BlockPos pos) {
        var entities = world.getOtherEntities(null, new Box(pos));
        for (var entity : entities) {
            if (entity.isSneaking()) continue;

            var ePos = entity.getPos();
            if (Math.abs(ePos.getY() - pos.getY() - 0.5f) > 1e-8) continue;

            float pushDirX, pushDirZ;

            success:
            {
                for (var region : beltRegions) {
                    if (ePos.x >= region.minX + pos.getX() && ePos.z >= region.minZ + pos.getZ()
                            && ePos.x <= region.maxX + pos.getX() && ePos.z <= region.maxZ + pos.getZ()) {
                        pushDirX = region.pushDirX;
                        pushDirZ = region.pushDirZ;
                        break success;
                    }
                }
                continue;
            }

            var minDistance = pos.getSquaredDistance(ePos.x, ePos.y, ePos.z, true);
            var pusherPos = pos;

            for (var i = 0; i < 4; i++) {
                var direction = Direction.fromHorizontal(i);
                var newPos = pos.offset(direction);
                if (world.getBlockEntity(newPos, BlockEntityRegistry.CONDUIT).map(c -> c.getPart("item")).isEmpty())
                    continue;

                var newDistance = newPos.getSquaredDistance(ePos.x, ePos.y, ePos.z, true);
                if (newDistance < minDistance) {
                    minDistance = newDistance;
                    pusherPos = newPos;
                }
            }

            if (pusherPos.equals(pos)) {
                var vel = 1f / 20f;
                entity.addVelocity(pushDirX * vel, 0f, pushDirZ * vel);
            }
        }
    }

    @Override
    public ItemConduitRenderData getRenderData() {
        return new ItemConduitRenderData(modelId, modelDirection);
    }

    @Override
    public NbtCompound writeNbt(NbtCompound tag) {
        for (var entry : connections.entrySet()) {
            tag.putByte(entry.getKey().name(), (byte) entry.getValue().ordinal());
        }

        return tag;
    }

    @Override
    public void readNbt(NbtCompound tag) {
        connections.clear();
        for (var direction : Direction.values()) {
            if (tag.contains(direction.name(), NbtElement.BYTE_TYPE)) {
                setConnection(direction, ConnectionKind.values()[tag.getByte(direction.name())]);
            }
        }
        update();
    }

    enum ConnectionKind {
        NONE,
        SRC_BELT,
        DST_BELT
    }

    static class BeltRegion {
        public float minX;
        public float minZ;
        public float maxX;
        public float maxZ;

        public float pushDirX;
        public float pushDirZ;

        public BeltRegion(float offset, float length, Direction dir, boolean flip) {
            minX = offset;
            maxX = offset + length;
            minZ = 0.375f;
            maxZ = 0.625f;

            switch (dir) {
                case NORTH -> setPositions(minZ, minX, maxZ, maxX);
                case SOUTH -> setPositions(minZ, 1f - maxX, maxZ, 1f - minX);
                case EAST -> setPositions(1f - maxX, minZ, 1f - minX, maxZ);
                case WEST -> setPositions(minX, minZ, maxX, maxZ);
            }

            pushDirX = dir.getUnitVector().getX();
            pushDirZ = dir.getUnitVector().getZ();

            if (flip) {
                pushDirX = -pushDirX;
                pushDirZ = -pushDirZ;
            }
        }

        private void setPositions(float minX, float minZ, float maxX, float maxZ) {
            this.minX = minX;
            this.minZ = minZ;
            this.maxX = maxX;
            this.maxZ = maxZ;
        }
    }
}
