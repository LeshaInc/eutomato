package inc.industries.eutomato.conduits;

import inc.industries.eutomato.conduits.item.ItemConduitPart;
import inc.industries.eutomato.conduits.item.ItemConduitRenderer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

public class ConduitPartRegistry {
    private static final Map<String, ConduitPartEntry> entries = new HashMap<>();

    static {
        register("item", ItemConduitPart::new, new ItemConduitRenderer());
    }

    public static void register(String id, Supplier<ConduitPart> constructor, ConduitPartRenderer renderer) {
        var entry = new ConduitPartEntry();
        entry.constructor = constructor;
        entry.renderer = renderer;
        entries.put(id, entry);
    }

    public static ConduitPartRenderer getRenderer(String id) {
        return entries.get(id).renderer;
    }

    public static ConduitPart newPart(String id) {
        return entries.get(id).constructor.get();
    }

    public static Set<String> getRegisteredIds() {
        return entries.keySet();
    }

    private static class ConduitPartEntry {
        public Supplier<ConduitPart> constructor;
        public ConduitPartRenderer renderer;
    }
}
