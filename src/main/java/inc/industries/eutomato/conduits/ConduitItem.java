package inc.industries.eutomato.conduits;

import inc.industries.eutomato.Eutomato;
import inc.industries.eutomato.registry.BlockRegistry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;

import java.util.Optional;

public class ConduitItem extends Item {
    public ConduitItem() {
        super(new FabricItemSettings().group(Eutomato.ITEM_GROUP));
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        var world = context.getWorld();
        var player = Optional.ofNullable(context.getPlayer());
        var blockPos = context.getBlockPos();
        var blockEntity = world.getBlockEntity(blockPos);

        var isSneaking = player.filter(Entity::isSneaking).isPresent();
        if (!isSneaking && blockEntity instanceof ConduitBlockEntity conduit) {
            var part = newPart(blockPos, context);
            if (world.intersectsEntities(null, getPartCollisionShape(part, blockPos))) {
                if (addPart(conduit, part) == ActionResult.SUCCESS) {
                    return ActionResult.SUCCESS;
                }
            }
        }

        var pos = blockPos.offset(context.getSide());
        var state = BlockRegistry.CONDUIT.getDefaultState();
        var part = newPart(pos, context);

        if (!world.intersectsEntities(null, getPartCollisionShape(part, pos))) {
            return ActionResult.FAIL;
        }

        world.setBlockState(pos, state);

        if (world.getBlockEntity(pos) instanceof ConduitBlockEntity conduit) {
            addPart(conduit, part);
        }

        return ActionResult.SUCCESS;
    }

    private static VoxelShape getPartCollisionShape(ConduitPart part, BlockPos pos) {
        return part.getCollisionShape().offset(pos.getX(), pos.getY(), pos.getZ());
    }

    protected ConduitPart newPart(BlockPos pos, ItemUsageContext context) {
        return null;
    }

    protected ActionResult addPart(ConduitBlockEntity conduit, ConduitPart newPart) {
        return ActionResult.FAIL;
    }
}
