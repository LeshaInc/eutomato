package inc.industries.eutomato.conduits;

import inc.industries.eutomato.registry.BlockEntityRegistry;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.fabricmc.fabric.api.rendering.data.v1.RenderAttachmentBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public class ConduitBlockEntity extends BlockEntity implements RenderAttachmentBlockEntity, BlockEntityClientSerializable {
    private final Map<String, ConduitPart> parts = new HashMap<>();

    public ConduitBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityRegistry.CONDUIT, pos, state);
    }

    public void addPart(String id, ConduitPart part) {
        parts.put(id, part);
    }

    public ConduitPart getPart(String id) {
        return parts.get(id);
    }

    public VoxelShape getCollisionShape() {
        return parts.values().stream()
                .map(ConduitPart::getCollisionShape)
                .reduce(VoxelShapes.empty(), VoxelShapes::union);
    }

    public static void tick(World world, BlockPos pos, BlockState state, BlockEntity self_) {
        var conduit = (ConduitBlockEntity) self_;
        for (var part : conduit.parts.values()) {
            part.tick(world, pos);
        }
    }

    private void writeNbtParts(NbtCompound tag) {
        for (var entry : parts.entrySet()) {
            tag.put(entry.getKey(), entry.getValue().writeNbt(new NbtCompound()));
        }
    }

    private void readNbtParts(NbtCompound tag) {
        for (var id : ConduitPartRegistry.getRegisteredIds()) {
            if (tag.contains(id, NbtElement.COMPOUND_TYPE)) {
                ConduitPart part;

                if (parts.containsKey(id)) {
                    part = parts.get(id);
                } else {
                    part = ConduitPartRegistry.newPart(id);
                    parts.put(id, part);
                }

                part.readNbt(tag.getCompound(id));
            }
        }
    }

    @Override
    public NbtCompound writeNbt(NbtCompound tag) {
        super.writeNbt(tag);
        writeNbtParts(tag);
        return tag;
    }

    @Override
    public void readNbt(NbtCompound tag) {
        super.readNbt(tag);
        readNbtParts(tag);
    }

    @Override
    public @Nullable Object getRenderAttachmentData() {
        return new ConduitRenderData(parts);
    }

    @Override
    public void fromClientTag(NbtCompound tag) {
        readNbtParts(tag);
    }

    @Override
    public NbtCompound toClientTag(NbtCompound tag) {
        writeNbtParts(tag);
        return tag;
    }
}
