package inc.industries.eutomato.conduits;

import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.fabricmc.fabric.api.rendering.data.v1.RenderAttachedBlockView;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Supplier;

public interface ConduitPartRenderer {
    default List<Identifier> getModelIdentifiers() {
        return List.of();
    }

    default List<SpriteIdentifier> getSpriteIdentifiers() {
        return List.of();
    }

    default void receiveModels(Map<Identifier, FabricBakedModel> models) {
    }

    default void receiveSprites(Map<SpriteIdentifier, Sprite> sprites) {
    }

    default void renderStatic(Object rawData, RenderAttachedBlockView blockView, BlockState state, BlockPos pos,
                              Supplier<Random> randomSupplier, RenderContext context) {
    }

    default void renderDynamic(ConduitBlockEntity conduit, float tickDelta, MatrixStack matrices,
                               VertexConsumerProvider vertexConsumers, int light, int overlay) {
    }
}
