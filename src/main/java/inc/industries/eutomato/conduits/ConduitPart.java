package inc.industries.eutomato.conduits;

import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.World;

public interface ConduitPart {
    VoxelShape getCollisionShape();

    void tick(World world, BlockPos pos);

    Object getRenderData();

    NbtCompound writeNbt(NbtCompound tag);

    void readNbt(NbtCompound tag);
}
