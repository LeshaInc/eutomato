package inc.industries.eutomato;

import inc.industries.eutomato.registry.BlockEntityRegistry;
import inc.industries.eutomato.registry.BlockRegistry;
import inc.industries.eutomato.registry.ItemRegistry;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Eutomato implements ModInitializer {
    public static final String MOD_ID = "eutomato";

    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);

    public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.create(new Identifier(MOD_ID, "general"))
            .icon(() -> new ItemStack(Blocks.COBBLED_DEEPSLATE))
            .build();

    @Override
    public void onInitialize() {
        LOGGER.info("Hello Minecraft!");

        ItemRegistry.register();
        BlockRegistry.register();
        BlockEntityRegistry.register();

        LOGGER.info("Initialization finished.");
    }
}