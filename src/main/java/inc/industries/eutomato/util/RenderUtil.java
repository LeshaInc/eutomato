package inc.industries.eutomato.util;

import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.util.math.Direction;

public class RenderUtil {
    public static void pushSimpleTransform(Direction direction, RenderContext context) {
        pushSimpleTransform(direction, 0f, 0f, 0f, context);
    }

    public static void pushSimpleTransform(Direction direction, float sx, float sy, float sz, RenderContext context) {
        context.pushTransform(q -> {
            for (int i = 0; i < 4; i++) {
                switch (direction) {
                    case NORTH -> q.pos(i, sx + q.z(i), sy + q.y(i), sz + 1 - q.x(i));
                    case SOUTH -> q.pos(i, sx + 1 - q.z(i), sy + q.y(i), sz + q.x(i));
                    case WEST -> q.pos(i, sx + 1 - q.x(i), sy + q.y(i), sz + 1 - q.z(i));
                    case EAST -> q.pos(i, sx + q.x(i), sy + q.y(i), sz + q.z(i));
                }
            }

            return true;
        });
    }
}
