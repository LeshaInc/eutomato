package inc.industries.eutomato.model;

import com.mojang.datafixers.util.Pair;
import inc.industries.eutomato.Eutomato;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.mesh.MutableQuadView;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("ALL")
public class ObjUnbakedModel implements UnbakedModel {
    private final ObjModelResourceProvider.QuadGroup[] quadGroups;

    public ObjUnbakedModel(ObjModelResourceProvider.QuadGroup[] quadGroups) {
        this.quadGroups = quadGroups;
    }

    @Override
    public Collection<Identifier> getModelDependencies() {
        return Collections.emptyList();
    }

    @Override
    public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter, Set<Pair<String, String>> unresolvedTextureReferences) {
        return Arrays.stream(quadGroups)
                .map(ObjModelResourceProvider.QuadGroup::sprite)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings rotationContainer, Identifier modelId) {
        var renderer = RendererAccess.INSTANCE.getRenderer();
        if (renderer == null) {
            Eutomato.LOGGER.error("Fabric renderer is not available");
            return null;
        }

        var defaultSprite = textureGetter.apply(new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, null));

        var meshBuilder = renderer.meshBuilder();
        var emitter = meshBuilder.getEmitter();

        for (var quadGroup : quadGroups) {
            for (var quad : quadGroup.quads()) {
                var color = quadGroup.spriteColor();
                var sprite = quadGroup.sprite() == null ? defaultSprite : textureGetter.apply(quadGroup.sprite());
                sprite = sprite == null ? defaultSprite : sprite;
                emitQuad(emitter, quad);
                emitter.spriteColor(0, color, color, color, color);
                emitter.spriteBake(0, sprite, MutableQuadView.BAKE_NORMALIZED | MutableQuadView.BAKE_FLIP_V);
                emitter.emit();
            }
        }

        var mesh = meshBuilder.build();
        return new ObjBakedModel(mesh, defaultSprite);
    }

    private void emitQuad(QuadEmitter emitter, ObjModelResourceProvider.Quad quad) {
        emitVertex(emitter, 0, quad.a());
        emitVertex(emitter, 1, quad.b());
        emitVertex(emitter, 2, quad.c());
        emitVertex(emitter, 3, quad.d());

    }

    private void emitVertex(QuadEmitter emitter, int index, ObjModelResourceProvider.QuadVertex vertex) {
        emitter.pos(index, vertex.x(), vertex.y(), vertex.z());
        emitter.normal(index, vertex.nx(), vertex.ny(), vertex.nz());
        emitter.sprite(index, 0, vertex.u(), vertex.v());
    }
}
