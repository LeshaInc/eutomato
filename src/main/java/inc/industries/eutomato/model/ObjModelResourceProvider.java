package inc.industries.eutomato.model;

import de.javagl.obj.*;
import inc.industries.eutomato.Eutomato;
import net.fabricmc.fabric.api.client.model.ModelProviderContext;
import net.fabricmc.fabric.api.client.model.ModelResourceProvider;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ObjModelResourceProvider implements ModelResourceProvider {
    private final ResourceManager resourceManager;

    public ObjModelResourceProvider(ResourceManager manager) {
        this.resourceManager = manager;
    }

    @Override
    public @Nullable UnbakedModel loadModelResource(Identifier id, ModelProviderContext context) {
        var path = "models/" + id.getPath();
        if (!path.endsWith(".obj")) {
            return null;
        }

        id = new Identifier(id.getNamespace(), path);

        try {
            return loadObj(id);
        } catch (Exception e) {
            Eutomato.LOGGER.error("Cannot load OBJ model from {}: {}", id, e);
            return null;
        }
    }

    private Map<String, Mtl> loadMaterials(Identifier objId, Obj obj) throws IOException {
        var materials = new LinkedHashMap<String, Mtl>();

        for (var fileName : obj.getMtlFileNames()) {
            var path = objId.getPath().replaceFirst("[^/]+\\.obj$", fileName);
            var id = new Identifier(objId.getNamespace(), path);
            var fileMaterials = MtlReader.read(resourceManager.getResource(id).getInputStream());
            for (var material : fileMaterials) {
                materials.put(material.getName(), material);
            }
        }

        return materials;
    }

    private ObjUnbakedModel loadObj(Identifier id) throws Exception {
        var stream = resourceManager.getResource(id).getInputStream();
        var obj = ObjReader.read(stream);
        var materials = loadMaterials(id, obj);
        var materialGroups = ObjSplitting.splitByMaterialGroups(obj);
        var quadGroups = new QuadGroup[materialGroups.size()];

        var i = 0;
        for (var entry : materialGroups.entrySet()) {
            var material = materials.get(entry.getKey());

            var sprite = material == null ? null :
                    new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, new Identifier(material.getMapKd()));

            var spriteColor = material == null ? 0xFFFFFFFF : 0xFF000000
                    | ((int) (material.getKd().getX() * 255f) << 16)
                    | ((int) (material.getKd().getY() * 255f) << 8)
                    | (int) (material.getKd().getZ() * 255f);

            var quads = objToQuads(entry.getValue());
            var quadGroup = new QuadGroup(quads, sprite, spriteColor);
            quadGroups[i++] = quadGroup;
        }

        return new ObjUnbakedModel(quadGroups);
    }

    private Quad[] objToQuads(Obj obj) throws Exception {
        var quads = new Quad[obj.getNumFaces()];

        for (int faceIdx = 0; faceIdx < obj.getNumFaces(); faceIdx++) {
            var face = obj.getFace(faceIdx);
            switch (face.getNumVertices()) {
                case 3 -> {
                    var dupVertex = QuadVertex.fromObj(obj, face, 0);
                    quads[faceIdx] = new Quad(
                            dupVertex,
                            dupVertex,
                            QuadVertex.fromObj(obj, face, 1),
                            QuadVertex.fromObj(obj, face, 2));
                }
                case 4 -> quads[faceIdx] = new Quad(
                        QuadVertex.fromObj(obj, face, 0),
                        QuadVertex.fromObj(obj, face, 1),
                        QuadVertex.fromObj(obj, face, 2),
                        QuadVertex.fromObj(obj, face, 3));
                default -> throw new Exception("n-gons are not supported");
            }
        }

        return quads;
    }

    record QuadVertex(float x, float y, float z,
                      float nx, float ny, float nz,
                      float u, float v) {
        public static QuadVertex fromObj(Obj obj, ObjFace face, int vertex) {
            var pos = obj.getVertex(face.getVertexIndex(vertex));
            var normal = obj.getNormal(face.getNormalIndex(vertex));
            var tex = obj.getTexCoord(face.getTexCoordIndex(vertex));
            return new QuadVertex(
                    pos.getX(), pos.getY(), pos.getZ(),
                    normal.getX(), normal.getY(), normal.getZ(),
                    tex.getX(), tex.getY());
        }
    }

    record Quad(QuadVertex a, QuadVertex b, QuadVertex c, QuadVertex d) {
    }

    record QuadGroup(Quad[] quads, SpriteIdentifier sprite, int spriteColor) {
    }
}
